rootProject.name = "resource-info"

val versionKotlin: String by settings
val versionSpringBoot: String by settings
val versionDependencyManagement: String by settings
val versionJib: String by settings

pluginManagement {
    plugins {
        kotlin("jvm") version versionKotlin
    }
    resolutionStrategy {
        eachPlugin {
            if (requested.id.id == rootProject.name) {
                useModule("ru.alse-code:${rootProject.name}:${requested.version}")
            }
        }
    }
}
