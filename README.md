### Gradle плагин resource-info
Пример простого градл плагина

#### *Подключение:*
в build.gradle(.kts)
```kotlin
plugins {
    id("resource-info") version "%version%"
}
```

В `settings.gradle.kts`
```kotlin
pluginManagement {
    repositories {
        gradlePluginPortal()
        maven("https://alse-code.ru/repository/maven-public/")
    }
}
```

#### *Использование:*
При подключенном плагине появляется раздел `plugin info tasks` в тасках gradle.  
В нём появляются таски для каждого файла в ресурсах
Эти таски выводят имя файла и его содержимое

#### *Настройка:*  
см. примеры в `ru.alse_code.ConfigurePluginTest`
