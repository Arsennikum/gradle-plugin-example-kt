@file:Suppress("UnstableApiUsage")

package ru.alse_code

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.compile.JavaCompile
import org.gradle.kotlin.dsl.create
import org.gradle.kotlin.dsl.withType
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.nio.charset.Charset

private const val extensionName = "pluginParams"
private const val filesPattern = "info/*.txt"
private const val pluginGroup = "plugin info tasks"

@Suppress("UNCHECKED_CAST", "UNUSED")
fun Project.pluginParams(func: PluginParamsExtension.() -> Unit) {
    val ext = extensions.getByName(extensionName) as? PluginParamsExtension
        ?: throw IllegalStateException("$extensionName is not of the correct type")
    ext.func()
}

class ResourceInfo : Plugin<Project> {
    override fun apply(project: Project) = with(project) {
        val generalTask = generateAllTask()

        val cfg = extensions.create<PluginParamsExtension>(extensionName)

        afterEvaluate {
            val resources = (properties["sourceSets"] as? SourceSetContainer)
                ?.getByName("main")
                ?.resources
            if (resources == null) {
                println("resources not found!")
                return@afterEvaluate
            }
            resources
                .asFileTree
                .matching {
                    it.include(filesPattern)
                }.forEach {

                    val fileName = it.nameWithoutExtension
                    generalTask.dependsOn += tasks.create("infoAbout_$fileName").apply {
                        group = pluginGroup
                        description = "print info for $fileName"

                        if (it.isDirectory) {
                            println("dir $fileName")
                        } else it.useLines(Charset.forName("UTF-8")) { fileLines ->
                            println("file $fileName content:")

                            if (cfg.oneLine) println(fileLines.firstOrNull())
                            else fileLines.forEach(::println)

                            if (cfg.enclosingLine) println("----------")
                        }
                    }
                }
        }
    }
}

private fun Project.generateAllTask(): Task {
    val generateAllTask = tasks.create("infoAll") {
        it.group = pluginGroup
        it.description = "print info for all files in $filesPattern"
    }

    tasks.withType<KotlinCompile> {
        dependsOn(generateAllTask)
    }
    tasks.withType<JavaCompile> {
        dependsOn(generateAllTask)
    }
    return generateAllTask
}
