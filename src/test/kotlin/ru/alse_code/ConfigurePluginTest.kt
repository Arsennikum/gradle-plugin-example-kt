package ru.alse_code

import io.kotlintest.should
import io.kotlintest.specs.WordSpec
import org.gradle.testkit.runner.GradleRunner
import org.gradle.testkit.runner.TaskOutcome
import java.nio.file.Files

class ConfigurePluginTest : WordSpec({

    "A configured Kotlin DSL builscript" should {
        "Lead to a successful build Kotlin DSL" {
            val projectDir = Files.createTempDirectory("")
            val buildScript = projectDir.resolve("build.gradle.kts").toFile()
            buildScript.writeText("""
                import ru.alse_code.pluginParams
                plugins {
                    kotlin("jvm") version "1.3.61"
                    id("resource-info")
                }
                pluginParams {
                    oneLine = false
                }
                tasks.create("example") {
                    dependsOn("infoAll")
                }
            """.trimIndent())

            val result = GradleRunner.create()
                .withProjectDir(projectDir.toFile())
                .withPluginClasspath()
                .withArguments("example", "--info", "--stacktrace")
                .build()

            result.task(":example") should {
                it != null && it.outcome == TaskOutcome.SUCCESS
            }
        }
    }

    "A configured Groovy DSL builscript" should {
        "Lead to a successful build Groovy DSL" {
            val projectDir = Files.createTempDirectory("")
            val buildScript = projectDir.resolve("build.gradle").toFile()
            buildScript.writeText("""
                plugins {
                    id 'resource-info'
                }
                pluginParams {
                    oneLine = false
                }
                tasks.create("example") {
                    dependsOn("infoAll")
                }
            """.trimIndent())

            val result = GradleRunner.create()
                .withProjectDir(projectDir.toFile())
                .withPluginClasspath()
                .withArguments("example", "--info", "--stacktrace")
                .build()

            result.task(":example") should {
                it != null && it.outcome == TaskOutcome.SUCCESS
            }
        }
    }
})
