package ru.alse_code

import io.kotlintest.shouldNotBe
import io.kotlintest.specs.WordSpec
import org.gradle.testfixtures.ProjectBuilder


class ResourceInfoTest : WordSpec({

    "Using the Plugin ID" should {
        "Apply the Plugin" {
            val project = ProjectBuilder.builder().build()
            project.pluginManager.apply("resource-info")

            project.plugins.getPlugin(ResourceInfo::class.java) shouldNotBe null
        }
    }


})
