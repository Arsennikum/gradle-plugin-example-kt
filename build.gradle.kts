import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
    id("maven-publish")
    id("java-gradle-plugin")
}

val appGroup: String by project
val appVersion: String by project
val appMainClass: String by project
val versionKotlin: String by project

project.group = appGroup
project.version = appVersion

repositories {
    mavenCentral()
}

dependencies {
    implementation(gradleKotlinDsl())
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:$versionKotlin")
    testImplementation("io.kotlintest:kotlintest-runner-junit5:3.3.1")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

gradlePlugin {
    plugins {
        create(project.name) {
            id = project.name
            implementationClass = "ru.alse_code.ResourceInfo"
        }
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

val sourceJar by tasks.creating(Jar::class) {
    archiveClassifier.set("sources")
    from(sourceSets["main"].allSource)
}

publishing {
    publications {
        create<MavenPublication>("default") {
            from(components["kotlin"])
            artifact(sourceJar)
        }
    }
    repositories {
        maven {
            url = if (appVersion.endsWith("-RELEASE")) {
                uri("https://alse-code.ru/repository/maven-releases/")
            } else {
                uri("https://alse-code.ru/repository/maven-snapshots/")
            }

            credentials {
                username = System.getenv("NEXUS_PUBLISHER_USER") ?: "snapshot"
                password = System.getenv("NEXUS_PUBLISHER_PASSWORD") ?: ""
            }
        }
    }
}
